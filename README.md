# js-hometask-4

## Ответьте на вопросы

1. Как можно создать промис?
- new Promise((resolve, reject) => {})
- создать выполненные промисы
Promise.resolve(result);
Promise.reject(new Error());

2. Что случится, когда опустеет стек вызовов?
память, выделенная под выполняемые функции, очищается, есть возможность перейти к выполнению следующего кода, например колбэков
3. Почему функции, переданные в setTimeout(), не начинают выполнение ровно через указанное время?
потому что есть очередь  и перед таймаутом должен текущий стек вызовов стать пустым
## Выполните задания

- Установи зависимости npm `install`;
- Допиши функции в `tasks.js`;
- Проверяй себя при помощи тестов `npm run test`;
- Создайте Merge Request с решением.
